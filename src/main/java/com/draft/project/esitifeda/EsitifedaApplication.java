package com.draft.project.esitifeda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EsitifedaApplication {

	public static void main(String[] args) {
		SpringApplication.run(EsitifedaApplication.class, args);
	}

}
